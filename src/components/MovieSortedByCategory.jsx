import React from 'react'

import movieCategory from '../data/movieCategories'
import movieList from '../data/movieList'
import { useParams } from 'react-router-dom'
import MovieListItem from './MovieListItem';
import Section from './Section';

function MovieSortedByCategory() {

  const { genreId } = useParams();

  return (
    <>
    <Section title={genreId}/>
      <div className="myList__movieContainer">
        {movieList.results.filter((eachMovie) =>
          eachMovie.genre_ids.indexOf(parseInt(genreId)) >= 0)
          .map((eachMovie) => (
            <MovieListItem
              key={eachMovie.id}
              poster={`https://www.themoviedb.org/t/p/w1280/${eachMovie.poster_path}`}
              title={eachMovie.title}
            />
          ))}
      </div>
    </>
  )
}

export default MovieSortedByCategory