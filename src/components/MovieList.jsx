import MovieListItem from "./MovieListItem";
import "../assets/styles/MovieList.css";
import { Link } from "react-router-dom";

function MovieList(props) {
  const { movieList } = props;


  return (
    <div className="MovieList__container">
      {movieList.map((movie) => (
        <Link to={`/details/${movie.id}`} key={movie.id}>
          <MovieListItem
            poster={`https://www.themoviedb.org/t/p/w1280/${movie.poster_path}`}
            title={movie.title}
            id={movie.id}
          />
        </Link>
      ))}
    </div>
  );
}

export default MovieList;

