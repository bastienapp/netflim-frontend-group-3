import { NavLink } from "react-router-dom";
import "../assets/styles/Navigation.css";

function Navigation() {
  return (
    <nav>
      <NavLink to="/Profile">
        <img src="src/assets/profil.png" alt="Symbole Profil" />
        <p className="NavLink-label">Profil</p>
      </NavLink>

      <NavLink to="/">
        <img src="src/assets/home.png" alt="Symbole accueil" />
        <p className="NavLink-label">Accueil</p>
      </NavLink>

      <NavLink to="/Search">
        <img src="src/assets/search.png" alt="Symbole recherche" />
        <p className="NavLink-label">Recherche</p>
      </NavLink>

      <NavLink to="/MyList">
        <img src="src/assets/bookmark-unfilled.png" alt="Symbole Liste" />
        <p className="NavLink-label">Ma liste</p>
      </NavLink>
    </nav>
  );
}

export default Navigation;
