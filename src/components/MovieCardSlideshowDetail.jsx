import React from 'react'

import '../assets/styles/movieCardSlideshowDetail.css'
import BookmarkToggle from './BookmarkToggle';

function MovieCardSlideshowDetail(props) {
    const { poster, title, id } = props;

    return (
        <>
            <div className="movieCard__container">
                <BookmarkToggle id={id} />
                <img className="movieCard__img" src={poster} alt={title} />
            </div>
        </>
    )
}

export default MovieCardSlideshowDetail