import "../assets/styles/MovieListItem.css";
import BookmarkToggle from "./BookmarkToggle";

function MovieListItem(props) {
  
  const { poster, title, id} = props;
  

  return (
    <div className="MovieListItem__card">
      <BookmarkToggle id={id} />
      <img src={poster} alt={title} className="MovieListItem__img" />
    </div>
  );
}

export default MovieListItem;
