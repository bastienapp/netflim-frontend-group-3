import { useState } from "react";

import checkmarkUnfilled from "../assets/checkmark-unfilled.png";
import checkmarkFilled from "../assets/checkmark-filled.png";

import "../assets/styles/checkmarkToggle.css";

function CheckmarkToggle(props) {
  const { id } = props;

  //récupère la valeur dans localStorage et la stocke dans la variable
  //comme la valeur dans localStorage est une string, on la convertit en booléen
  //dans le cas où aucune valeur existe, renvoie "null" qui n'est pas égal à "true" donc quand même faux
  const currentState =
    localStorage.getItem(`${id}.isSeen`) === "true" ? true : false;

  const [seen, setSeen] = useState(currentState);

  function checkmarkToggle() {
    setSeen(!seen);

    localStorage.setItem(`${id}.isSeen`, !seen);
  }

  return (
    <div className="checkmark--container">
      {/* useState to toggle state Favorite */}
      <input
        onClick={checkmarkToggle}
        className="checkmark--icon"
        src={seen ? checkmarkFilled : checkmarkUnfilled}
        type="image"
      />
    </div>
  );
}

export default CheckmarkToggle;
