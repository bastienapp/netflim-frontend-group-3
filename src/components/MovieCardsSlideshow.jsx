import React from 'react'
import MovieCardSlideshowDetail from './MovieCardSlideshowDetail';

import '../assets/styles/movieCardSlideshow.css'

import SeeMoreBtn from './SeeMoreBtn';
import { Link } from "react-router-dom";

function MovieCardsSlideshow(props) {
    const { movieList, path } = props;

    return (
      <>
        <div className="slider__container">
          <div className="movieCardSlideshow__container">
            <div className="marginDiv"></div>
            {/* .slice limits the number of items */}
            {movieList.slice(0, 10).map((eachMovie) => (
              <Link to={`/details/${eachMovie.id}`} key={eachMovie.id}>
                <MovieCardSlideshowDetail
                  poster={
                    "https://www.themoviedb.org/t/p/w1280/" +
                    eachMovie.poster_path
                  }
                  title={eachMovie.original_title}
                  id={eachMovie.id}
                />
              </Link>
            ))}
            <SeeMoreBtn path={path}/>
          </div>
        </div>
      </>
    );
}

export default MovieCardsSlideshow