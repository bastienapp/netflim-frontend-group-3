import React from 'react'

import '../assets/styles/seeMoreBtn.css'
import chevronRight from '../assets/chevron-right.png'
import { Link } from 'react-router-dom'

export default function SeeMoreBtn(props) {
    const { path } = props
    return (
        <>
            <div className="btnSeeMore__container">
                <Link to={path}>
                    <button className="btnSeeMore">
                        <img className="chevronIcon" src={chevronRight}></img>
                    </button>
                </Link>
            </div >
        </>
    )
}
