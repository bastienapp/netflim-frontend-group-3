import "../assets/styles/Header.css";
import { Link } from "react-router-dom";

const Header = (props) => {

  const { featuredMovie } = props;


  return (
    <header className="Header">
      <img
        className="Header__background-img"
        src={`https://www.themoviedb.org/t/p/w1280/${featuredMovie.backdrop_path}`}
        alt={featuredMovie.original_title}
      />

      <div className="Header__logo-container">
        <img
          className="Header__logo-container--logo"
          src="src/assets/logoNetflim.png"
          alt="Logo Netflim"
        />
      </div>

      <div className="Header__movie-info">
        <h3 className="Header__movie-info--title">
          {featuredMovie.original_title}
        </h3>
        <p className="Header__movie-info--year">
          ({featuredMovie.release_date.substring(0, 4)})
        </p>
      </div>

      <Link to={`/details/${featuredMovie.id}`} key={featuredMovie.id}>
        <button className="Header__movie-button">Découvrir</button>
      </Link>
    </header>
  );
 
};

export default Header;
