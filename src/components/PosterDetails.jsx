import MovieListItem from "./MovieListItem";
import "../assets/styles/PosterDetails.css";
import Rating from "./Rating";

//TODO ajouter le composant <MovieListItem/> et <Rating>. Aller chercher les infos avec useParams

function PosterDetails(props) {

  const { poster, title , rating, id} = props;

    return (
      <>
        <div className="PosterDetails">

          <MovieListItem poster={poster} title={title} id={id} />
          
          <Rating rating= {rating} />
        </div>
      </>
    );
}

export default PosterDetails;
