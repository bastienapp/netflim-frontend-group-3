import SectionTitle from "./SectionTitle";


function Section(props) {
  const { title, content } = props;

  return (
    <section>
      <SectionTitle title={title} />
      <div>{content}</div>
    </section>
  );
}

export default Section;
