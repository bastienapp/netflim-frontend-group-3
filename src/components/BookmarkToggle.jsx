import { useState} from "react";
import bookmarkUnfilled from "../assets/bookmark-unfilled.png";
import bookmarkFilled from "../assets/bookmark-filled.png";
import "../assets/styles/bookmarkToggle.css";

function BookmarkToggle(props) {

  const { id } = props

  /*récupération l'ID du film grace aux props*/
  // const ID = 7612; 
  
  /*récuprétion de ce qu'il y a dans localstorage */
  const currentState = localStorage.getItem(`${ id }.isBookmarked`) === "true" ? true : false;
  
  /* création du state de l'état du toggle avec valeur par défault l'état qu'il y a dans local storage */
  const [bookmarked, setBookmarked] = useState(currentState);

  /* création de la fonction toggle, qui sera appelé dans l'input. Cette fonction fait la bascule du state (true à false, ou false à true)*/
  function toggleState() {
    setBookmarked(!bookmarked);
    // L'etat n'a pas encore changé, on met manuellement la valeur à l'inverse
    localStorage.setItem(`${id}.isBookmarked`, !bookmarked);
  }

  return (
    <div className="bookmarkComponent__container">
      <div className="movieCard__bookmarkIcon__iconContainer">
        <input
          onClick={toggleState}
          className="movieCard__bookmarkIcon"
          src={bookmarked ? bookmarkFilled : bookmarkUnfilled}
          type="image"
        />
      </div>
    </div>
  );
}

export default BookmarkToggle;
