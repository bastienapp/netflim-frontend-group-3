import "../assets/styles/Rating.css";

function Rating(props) {

    //TODO aller chercher le vote_average associé au film cliqué grâce à useParams. Convertir la note sur 5.
  return (
    <div className="Rating-container">
      {props.rating} <span>/10</span>
    </div>

    
  );
}

export default Rating;
