import { useState, useEffect } from "react";
import { Rating } from "react-simple-star-rating";

function PersonnalRating(props) {
  const { id } = props;

  const [rating, setRating] = useState(0);

  // Charge la note depuis localStorage lors du 1er affichage du composant
  useEffect(() => {
    const storedRating = localStorage.getItem(`${id}.rating`);
    if (storedRating) {
      setRating(parseFloat(storedRating));
    }
  }, [id]);

  // Met à jour la note et sauvegarde dans le localStorage
  const handleRating = (personalRate) => {
    setRating(personalRate);
    localStorage.setItem(`${id}.rating`, personalRate.toString());
  };
  
  //TODO : faire en sorte que l'affichage des étoiles reste selon la note sauvegardée

  return (
    <Rating
      ratingValue={rating}
      onClick={handleRating}
      allowFraction
      fillColor="#DD960E"
    />
  );
}

export default PersonnalRating;
