import "../assets/styles/SectionTitle.css";

function SectionTitle(props) {
  return <h2>{props.title}</h2>;
}

export default SectionTitle;
