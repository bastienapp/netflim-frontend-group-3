import "../assets/styles/Footer.css";
import Navigation from "../components/Navigation"

const Footer = () => {
  return (
    <>
      <footer>
        <Navigation />
      </footer>
    </>
  );
};

export default Footer;
