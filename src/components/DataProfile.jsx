import "../assets/styles/DataProfile.css"

function DataProfile(props) {
    const {content,title} =props
    return (
      <section className="content">
        <div>{title}</div>
        <div className="boxcontent">{content}</div>
      </section>
    );
}
export default DataProfile
