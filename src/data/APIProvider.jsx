import axios from 'axios'
import APIContext from "./Context";
import { useEffect, useState } from 'react'

function APIProvider({ children }) {

    const [popularMovieAPI, setPopularMovieAPI] = useState([])

    const options = {
        method: 'GET',
        headers: {
            accept: 'application/json',
            Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4ZTRkNWE2NjU3ZmYwZjM5MDlkY2I5ZWJhMTZlYTFkMiIsInN1YiI6IjY1N2IxMzk5N2VjZDI4MDEzYjNlYmJlZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.2ZqSKUgRMzHt-U6HTkAr5OqHQcVa1r2m3zWRtosXYUE'
        }
    };

    useEffect(() => {
        axios.get('https://api.themoviedb.org/3/movie/popular?language=en-US&page=1', options)
            .then((response) => {
                setPopularMovieAPI(response.data.results)
            })
    }, [])

    return (
        <APIContext.Provider value={popularMovieAPI} >
            {children}
        </APIContext.Provider>
    )
}

export default APIProvider