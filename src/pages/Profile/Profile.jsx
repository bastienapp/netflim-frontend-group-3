import DataProfile from "../../components/DataProfile";
import Footer from "../../components/Footer";
import MovieCardsSlideshow from "../../components/MovieCardsSlideshow";
import movieList from "../../data/movieList";
import Section from "../../components/Section";
import '../../assets/styles/Profile.css'


const Profile = () => {


  // Display a random movie in the Header
  const getRandom = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  };

  return (
    <>
      <div className="headerProfil">

        <div className="Header_container">
          <img
            className="Header__background-img"
            src={`https://www.themoviedb.org/t/p/w1280/${
              movieList.results[getRandom(0, movieList.results.length)]
                .backdrop_path
            }`}
            alt="Image film"
          />
          <img
            className="ProfilPicture"
            src="src/assets/profil.png"
            alt="Avatar Profil"
          />
        </div>

        <img
          className="Parameters"
          src="src/assets/parameter.png"
          alt="parameter button"
        />

        <p className="Profile_pseudo">Mon pseudo</p>

      </div>

      <div className="dataContent">
        <div className="content">
          <DataProfile title="Catégorie Préférée" content="Comédie" />
        </div>

        <div className="content">
          <DataProfile title="Nombre de films vus" content="215" />
        </div>

        <div className="content">
          <DataProfile title="Inscrit depuis le" content="05/01/2019" />
        </div>

        <div className="content">
          <DataProfile title="Nombre de films notés" content="36" />
        </div>
      </div>

      
      <Section
        title="Ma liste"
        content={
          <MovieCardsSlideshow movieList={movieList.results} path="/MyList" />
        }
      />
      <Section
        title="Derniers films vus"
        content={<MovieCardsSlideshow movieList={movieList.results} path="/" />}
      />
      <Section
        title="Mes films les mieux notés"
        content={<MovieCardsSlideshow movieList={movieList.results} path="/" />}
      />
      <Footer />;
    </>
  );
};

export default Profile;
