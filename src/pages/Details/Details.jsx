import { useContext } from 'react'
import APIContext from '../../data/Context';
import "../../assets/styles/details.css";
import PosterDetails from "../../components/PosterDetails";
import close from "../../assets/close.png";
import CheckmarkToggle from "../../components/CheckmarkToggle";
import movieList from "../../data/movieList";
import { useParams } from "react-router";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import PersonnalRating from "../../components/PersonnalRating";

function Details() {

  const APIData = useContext(APIContext)

  /*retour en arrière à la fermeture de details*/

  const navigate = useNavigate();

  /* Récupérer les infos du films selon l'id */

  const { id } = useParams();
  const [movie, setMovie] = useState(null);

  useEffect(() => {
    //cible le film dans le tableau movieList.results grâce à l'id récupéré de l'url (les deux sont des strings)
    const selectedMovie = APIData.find(
      (movie) => movie.id.toString() === id
    ); //retourne le film d'id sélectionné

    //récupère et stocke le film dans le state si n'est pas undefined
    if (selectedMovie) {
      setMovie(selectedMovie);
    }
  }, [id]);

  /*traduire la catégorie du film */

  function translateCategory(arr) {
    let categoryArray = []; //stocke les catégories traduites
    let category;

    arr.forEach((id) => {
      switch (id) {
        case 28:
          category = "Action";
          break;
        case 12:
          category = "Adventure";
          break;
        case 16:
          category = "Animation";
          break;
        case 35:
          category = "Comedy";
          break;
        case 80:
          category = "Crime";
          break;
        case 99:
          category = "Documentary";
          break;
        case 18:
          category = "Drama";
          break;
        case 10751:
          category = "Family";
          break;
        case 14:
          category = "Fantasy";
          break;
        case 36:
          category = "History";
          break;
        case 27:
          category = "Horror";
          break;
        case 10402:
          category = "Music";
          break;
        case 9648:
          category = "Mystery";
          break;
        case 10749:
          category = "Romance";
          break;
        case 878:
          category = "Science-fiction";
          break;
        case 10770:
          category = "TV Movie";
          break;
        case 53:
          category = "Thriller";
          break;
        case 10752:
          category = "War";
          break;
        case 37:
          category = "Western";
          break;

        default:
          category = "Autre";
      }
      categoryArray.push(category);
    });

    return categoryArray.join(", ");
  }

  /*fin traduire catégories*/

  return (
    <div className="Details-container">
      <button
        className="Details__close-button"
        onClick={() =>
          navigate(-1)
        } /* permet de revenir à la page précédente hook de React Router useNavigate */
      >
        <img src={close} alt="close popup" />
      </button>

      {/* Permet d'attendre que tout soit chargé avant d'afficher */}

      {movie ? (
        <>
          <PosterDetails
            poster={`https://www.themoviedb.org/t/p/w1280/${movie?.poster_path}`}
            title={movie?.original_title}
            rating={movie?.vote_average}
            id={movie.id}
          />
          <div className="Details__mainInfos">
            <div className="Details__mainInfos--title-container">
              <h2 className="Details__mainInfos--title">
                {movie?.original_title}{" "}
                <span className="Details__mainInfos--year">
                  ({movie?.release_date.substring(0, 4)})
                </span>
              </h2>
            </div>

            <div className="Details__mainInfos--check">
              <CheckmarkToggle id={movie.id} />
            </div>
            <h4 className="Details__mainInfos--category">
              {translateCategory(movie.genre_ids)}
            </h4>
            <div className="Details__mainInfos--personnalRating">
              <PersonnalRating id={movie.id} />
            </div>
          </div>

          <div className="Details__synopsis">
            <h3 className="Details__synopsis--title">Synopsis</h3>
            <p className="Details__synopsis--content">{movie.overview}</p>
          </div>
        </>
      ) : (
        <p>Chargement en cours...</p>
      )}
    </div>
  );
}

export default Details;
