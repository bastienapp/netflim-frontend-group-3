
import filter from "../../assets/filter.png";
import logoNetflim from "../../assets/logoNetflim.png";
import "../../assets/styles/myList.css";
import SectionTitle from "../../components/SectionTitle";
import "../../assets/styles/MovieList.css";
import MovieCategorySlider from './MovieCategorySlider'
import movieList from "../../data/movieList";
import MovieListItem from "../../components/MovieListItem";
import Footer from "../../components/Footer";

import { Link } from 'react-router-dom';
import Header from '../../components/Header';
import { useContext } from "react";

import APIContext from "../../data/Context";

function MyList() {

  const storedBookmarkedMovie = [];

  for (let i = 0; i < localStorage.length; i++) {
    let key = localStorage.key(i);

    //si isBookmarkedKey est true et si la valeur de la clé vaut "true" alors :
    if (isBookmarkedKey(key) && localStorage.getItem(key) === "true") {
      storedBookmarkedMovie.push(key.split(".")[0]); // récupère les id des films bookmarked
    }
  }

  //vérifie si la clé correspond bien à celle de isBookmarked (retourne true ou false)
  function isBookmarkedKey(key) {
    const split = key.split(".");
    return split[1] === "isBookmarked";
  }

  const APIdata = useContext(APIContext);

  //récupère dans un tableau les données des films qui ont isBookmarked ="true"
  const movieInfosBookmarked = APIdata.filter((eachMovie) =>
    storedBookmarkedMovie.includes(String(eachMovie.id))
  );

  // Display a random movie in the Header
  const getRandom = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min)
  }

  return (
    <>
      <div className="MyList">
        <div className="myList__headerContainer">
          <Header
            featuredMovie={
              movieList.results[getRandom(0, movieList.results.length)]
            }
          />
        </div>
        <div>
          <MovieCategorySlider />
        </div>
        <div className="titleAndFilter__container">
          <SectionTitle title="Ma liste de films" />
          <input src={filter} type="image" className="filterIcon" />
        </div>
        <div className="myList__movieContainer">
          {movieInfosBookmarked.map((eachMovie) => (
            <Link to={`/details/${eachMovie.id}`} key={eachMovie.id}>
              <MovieListItem
                key={eachMovie.id}
                poster={`https://www.themoviedb.org/t/p/w1280/${eachMovie.poster_path}`}
                title={eachMovie.title}
                id={eachMovie.id}
              />
            </Link>
          ))}
        </div>
      </div>

      <Footer />
    </>
  );
}

export default MyList;
