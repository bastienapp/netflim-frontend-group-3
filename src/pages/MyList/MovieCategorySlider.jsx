import React from 'react'

import '../../assets/styles/movieCategorySlider.css'

import movieCategory from '../../data/movieCategories'
import { Link } from 'react-router-dom'

function MovieCategorySlider() {

    return (
        <>
            <div className="myList__CategorySlider__container">
                <div className="myList__CategorySlider">
                    {movieCategory.map((eachCategory) => (
                        <li key={eachCategory.id}>
                            <Link to={"/MyList/"+eachCategory.id} className="categoryName--removeLinkAppearance">
                                {eachCategory.name}
                            </Link>
                        </li>
                    ))}
                </div>
            </div >
        </>
    )
}

export default MovieCategorySlider