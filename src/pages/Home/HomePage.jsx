import { useContext } from 'react'

import APIContext from '../../data/Context';
import MovieCardsSlideshow from '../../components/MovieCardsSlideshow';
import "../../assets/styles/HomePage.css";
import movieList from "../../data/movieList";
import MovieList from "../../components/MovieList";
import Section from "../../components/Section";
import Header from "../../components/Header";
import Footer from "../../components/Footer";

function HomePage() {

  const storedBookmarkedMovie = [];

    for (let i=0; i<localStorage.length; i++) {
      let key = localStorage.key(i);

      //si isBookmarkedKey est true et si la valeur de la clé vaut "true" alors :
      if (isBookmarkedKey(key) && localStorage.getItem(key) === "true") {
        storedBookmarkedMovie.push(key.split(".")[0]); // récupère les id des films bookmarked
      }
      
  }

  //vérifie si la clé correspond bien à celle de isBookmarked (retourne true ou false)
  function isBookmarkedKey(key) {
    const split = key.split(".");
    return split[1] === "isBookmarked" 
  }

  const APIdata = useContext(APIContext);
 
  //récupère dans un tableau les données des films qui ont isBookmarked ="true"
  const movieInfosBookmarked = APIdata.filter(
    (eachMovie) => storedBookmarkedMovie.includes(String(eachMovie.id))
  );


  // Display a random movie in the Header
  const getRandom = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min)
  }

  return (
    <>
      <Header
        featuredMovie={
          movieList.results[getRandom(0, movieList.results.length)]
        }
      />

      <main className="HomePage">
        <Section
          title="Ma liste"
          content={
            <MovieCardsSlideshow
              movieList={movieInfosBookmarked}
              path="/MyList"
            />
          }
        />
        <Section
          title="Populaire"
          content={<MovieCardsSlideshow movieList={APIdata} path="/" />}
        />
        <Section
          title="Tous les films"
          content={<MovieList movieList={APIdata} />}
        />
      </main>
      <Footer />
    </>
  );
}

export default HomePage;
