import React from 'react'

import '../../assets/styles/myList.css'
import '../../assets/styles/search.css'

import movieList from "../../data/movieList";

import SectionTitle from '../../components/SectionTitle'
import Footer from '../../components/Footer'

import { useState } from 'react'
import MovieListItem from '../../components/MovieListItem';
import Header from '../../components/Header';

// import SearchBar from './SearchBar';

function Search() {

    const [search, setSearch] = useState('')

    const handleSearchChange = (event) => {
        setSearch(event.target.value)
    }

    // Display a random movie in the Header
    const getRandom = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min)
    }

    return (
        <>
            <Header featuredMovie={movieList.results[getRandom(0, movieList.results.length)]} />
            <div className="search__titleAndSearchBar__container">
                <SectionTitle title={search === "" ? "Recherche" : `Résultats pour "${{search}}"`} />
                <input id="searchBar" name="searchBar" type="search"
                    onChange={handleSearchChange}
                    value={search} placeholder=""
                />
            </div>
            <div className="search__resultContainer">
                {movieList.results.filter((eachMovie) =>
                    eachMovie.title.toLowerCase().includes(search.toLowerCase())
                )
                    .map((eachMovie) => (
                        <MovieListItem
                            key={eachMovie.id}
                            poster={`https://www.themoviedb.org/t/p/w1280/${eachMovie.poster_path}`}
                            title={eachMovie.title}
                        />
                    ))}
            </div>
            <Footer />
        </>
    )
}

export default Search