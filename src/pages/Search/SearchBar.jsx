import React from 'react'
import SectionTitle from '../../components/SectionTitle'

function SearchBar() {

    const [search, setSearch] = useState('')

    const handleSearchChange = (event) => {
        setSearch(event.target.value)
    }

    return (
        <>
            <div className="search__titleAndSearchBar__container">
                <SectionTitle title={search === "" ? "Recherche" : `Résultats pour "${{search}}"`} />
                <input id="searchBar" name="searchBar" type="search"
                    onChange={handleSearchChange}
                    value={search} placeholder=""
                />
            </div>
        </>
    )
}

export default SearchBar