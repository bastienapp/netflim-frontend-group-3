import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { useState, useEffect } from 'react'
import HomePage from "./pages/Home/HomePage.jsx";
import Search from "./pages/Search/Search.jsx";
import MyList from "./pages/MyList/MyList.jsx";
import Profile from "./pages/Profile/Profile.jsx";
import Details from "./pages/Details/Details";
import ErrorPage from "./pages/ErrorPage/ErrorPage.jsx";
import MovieSortedByCategory from "./components/MovieSortedByCategory";
import APIProvider from "./data/APIProvider";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/Profile",
    element: <Profile />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/Search",
    element: <Search />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/MyList",
    element: <MyList />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/MyList/:genreId",
    element: <MovieSortedByCategory />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/details/:id",
    element: <Details />,
    errorElement: <ErrorPage />,
  },
]);



ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <APIProvider>
      <RouterProvider router={router} />
    </APIProvider>
  </React.StrictMode>
);
